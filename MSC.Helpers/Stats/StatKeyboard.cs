﻿using System;
using System.Collections.Generic;
using Corale.Colore.Core;
using Corale.Colore.Razer.Keyboard;

namespace MSC.Helpers.Stats {
    public class StatKeyboard {

        private static List<Key> statKeys;

        private string username;

        private string stat;

        public StatKeyboard(string username, string stat) {
            this.username = username;
            this.stat = stat;
        }

        /// <summary>
        /// Gets a specific stat key (1,2,3,4,5,6,7,8,9,0) 
        /// </summary>
        /// <param name="keyIndex">Key that we want to get</param>
        /// <returns>Stat key at the index that we supplied</returns>
        public Key this[int keyIndex] {
            get {
                return GetStatKeys()[keyIndex];
            }
        }

        /// <summary>
        /// Gets all of the stat keys that the application can affect
        /// </summary>
        /// <returns>List of keys that we can change and apply stats to</returns>
        public static List<Key> GetStatKeys() {
            if (statKeys == null) {
                statKeys = new List<Key>() {
                    Key.One,
                    Key.Two,
                    Key.Three,
                    Key.Four,
                    Key.Five,
                    Key.Six,
                    Key.Seven,
                    Key.Eight,
                    Key.Nine,
                    Key.Zero
                };
            }
            return statKeys;
        }

        /// <summary>
        /// Gets the username of the person that we are going to get stats for
        /// </summary>
        /// <returns>Username of person</returns>
        public string GetUsername() {
            return username;
        }

    }
}
