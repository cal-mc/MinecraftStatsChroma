﻿using MSC.Helpers.Network;
using System;

namespace MSC.Helpers.Stats {
    public class Statistic {

        private string statisticCommand = "scoreboard players list {username}";

        public string Username { get; set; }

        public string Name { get; set; }

        public RCON Server { get; set; }

        public Client GetClient { get; set; }

        public int Value { get; private set; }

        public Statistic(string username, string name, RCON server) : this(username, name, server, null) { }

        public Statistic(string username, string name, Client client) : this(username, name, null, client) { }

        public Statistic(string username, string name, RCON server, Client client) {
            Username = username;
            Name = name;
            Server = server;
            GetClient = client;
            Value = 0;
            statisticCommand = statisticCommand.Replace("{username}", Username);
        }

        public Statistic Get() {
            string responce = null;

            if (GetClient != null) {
                responce = GetClient.SendData("RCON COMMAND:" +statisticCommand);
            } else if (Server != null) {
                responce = Server.SendCommand(statisticCommand);
            } else {
                return this;
            }

            if (responce == null || !responce.Contains(Name) || !responce.Contains(":")) {
                return this;
            }

            responce = responce.Split(new string[] { Name + ": " }, StringSplitOptions.None)[1].Split(' ')[0];

            int value = 0;

            if (int.TryParse(responce, out value)) {
                Value = value;
            }

            return this;
        }
    }
}
