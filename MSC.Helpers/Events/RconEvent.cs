﻿using MSC.Helpers.Events.Base;
using MSC.Helpers.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSC.Helpers.Events {
    class RconEvent : Event {
   
        public RCON RconServer { get; private set; }

        public RconEvent(RCON rconServer) {
            RconServer = rconServer;
        }

        public void Fire(string commandFeedback) {
            RconEventArgs eventArgs = new RconEventArgs(commandFeedback, RconServer);
        }

    }
}
