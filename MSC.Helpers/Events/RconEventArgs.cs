﻿using MSC.Helpers.Events.Base;
using MSC.Helpers.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSC.Helpers.Events {
    class RconEventArgs : CustomEventArgs {

        public RconEventArgs(string commandFeedback, RCON rconServer) : base() {
            CommandFeedback = commandFeedback;
            RconServer = rconServer;
        }

        public string CommandFeedback { get; private set; }

        public RCON RconServer { get; private set; }


    }
}
