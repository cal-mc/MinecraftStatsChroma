﻿using System;

namespace MSC.Helpers.Events.Base {
    public abstract class Event {

        public event Handler EventHandler;

        public delegate void Handler(Event e, CustomEventArgs eventArgs);

    }
}
