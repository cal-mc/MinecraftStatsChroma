﻿using System;

namespace MSC.Helpers.Events.Base {
    public class CustomEventArgs : EventArgs {

        public string Name { get; set; }

        public Guid ID { get; private set; }

        public bool IsCancelled { get; set; }

        public CustomEventArgs() {
            ID = Guid.NewGuid();
            Name = "Custom Event " + ID.ToString();
            IsCancelled = false;
        }

        public void SetCancelled(bool toCancel) {
            IsCancelled = toCancel;
        }

    }
}