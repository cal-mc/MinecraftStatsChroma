﻿using MSC.Helpers.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSC.Helpers.Listeners.Base {
    public interface IEventListener {

        void Listen();

        void Callback();

    }
}
