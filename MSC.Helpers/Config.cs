﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSC.Helpers {
    public class Config {

        // The filepath of the config file
        private string filePath;

        // The default extention to use on files if one is not supplied
        private static string defaultFileExtention = ".txt";

        // The key and value seperator for use in config files
        private static string fileSeperator = ":";

        // Config file contents (per line)
        private HashSet<string> contents = null;

        public Config(string filePath) {
            this.filePath = fixFilePath(filePath);
            save();
        }

        /// <summary>
        /// Fixes the filepath (within a data folder) of the Config file we are reading/writing/modifying
        /// </summary>
        /// <param name="filePath">The filepath that we currently have inlcuding FULL path AND file name</param>
        /// <returns>Filepath with a data folder inside of it and correct file extentions</returns>
        private string fixFilePath(string filePath) {
            if (!filePath.Contains(Directory.GetCurrentDirectory())) {
                filePath = Directory.GetCurrentDirectory() + "\\" + filePath;
            }

            // The true, computer friendly size of the array
            int size = filePath.Clone().ToString().Split('\\').Length - 1;

            // Get all of the folders and files by sperating them with a '\'
            string[] paths = filePath.Split('\\');


            // Get the last index in the array because we know that is going to be the file's name that we want to use
            string fileName = paths[size];

            // Holds the new filePath
            StringBuilder pathBuilder = new StringBuilder();

            // To count where we are in the array
            int index = 0;

            // Loop through all of the paths, if it is the last string, we know thats the name so break out of the loop
            foreach (string path in paths) {
                if (index == size) {
                    break;
                }
                pathBuilder.Append(path + "\\");

                index++;
            }

            // Get the new filepath from the correct file path builder
            filePath = pathBuilder.ToString();

            // Check to see if the file contains a data folder already..
            if (!filePath.Contains("\\data\\")) {
                if (!filePath.EndsWith("\\")) {
                    filePath = filePath + "\\";
                }
                filePath = filePath + "data\\";
            }

            // Make sure data of the directory exists...
            if (!Directory.Exists(filePath)) {
                Directory.CreateDirectory(filePath);
            }

            // Make sure the filename has the right extention
            if (!fileName.Contains(".")) {
                fileName = fileName + defaultFileExtention;
            }

            filePath = filePath + fileName;

            // If the file does not exist, create one
            if (!File.Exists(filePath)) {
                File.Create(filePath).Close();
            }

            // Return the path of the file + name (with extention) giving the correct file path
            return filePath;
        }

        /// <summary>
        /// Gets the filepath of the config file
        /// </summary>
        /// <returns>Config file filepath</returns>
        public string getFilePath() {
            return filePath;
        }

        /// <summary>
        /// Check to see if the config file exists
        /// </summary>
        /// <returns>True: Yes, False: No</returns>
        public bool configExists() {
            return (getContents() != null && getContents().Count != 0);
        }

        /// <summary>
        /// Gets a short from the config file using a key
        /// </summary>
        /// <param name="key">They key that we are using to find the short</param>
        /// <returns>The short that belongs to the key</returns>
        public int getShort(string key) {
            string value = getString(key);
            if (value == null || !NumberHelper.isNumber(value)) {
                return 0;
            }

            return short.Parse(value);
        }

        /// <summary>
        /// Sets and saves a key and its short value to the config file
        /// </summary>
        /// <param name="key">The key that will be used to in future get the short value provided</param>
        /// <param name="value">The short value that will be returned when retrieved by the key</param>
        /// <returns>Instance of the current config class</returns>
        public Config setShort(string key, short value) {
            return setString(key, value.ToString());
        }

        /// <summary>
        /// Gets an int from the config file using a key
        /// </summary>
        /// <param name="key">They key that we are using to find the int</param>
        /// <returns>The int that belongs to the key</returns>
        public int getInt(string key) {
            string value = getString(key);
            if (value == null || !NumberHelper.isNumber(value)) {
                return 0;
            }

            return int.Parse(value);
        }

        /// <summary>
        /// Sets and saves a key and its int value to the config file
        /// </summary>
        /// <param name="key">The key that will be used to in future get the int value provided</param>
        /// <param name="value">The int value that will be returned when retrieved by the key</param>
        /// <returns>Instance of the current config class</returns>
        public Config setInt(string key, int value) {
            return setString(key, value.ToString());
        }

        /// <summary>
        /// Gets a long from the config file using a key
        /// </summary>
        /// <param name="key">They key that we are using to find the long</param>
        /// <returns>The long that belongs to the key</returns>
        public long getLong(string key) {
            string value = getString(key);
            if (value == null || !NumberHelper.isNumber(value)) {
                return 0;
            }

            return long.Parse(value);
        }

        /// <summary>
        /// Sets and saves a key and its long value to the config file
        /// </summary>
        /// <param name="key">The key that will be used to in future get the long value provided</param>
        /// <param name="value">The long value that will be returned when retrieved by the key</param>
        /// <returns>Instance of the current config class</returns>
        public Config setLong(string key, long value) {
            return setString(key, value.ToString());
        }

        /// <summary>
        /// Gets an int from the config file using a key
        /// </summary>
        /// <param name="key">They key that we are using to find the int</param>
        /// <returns>The int that belongs to the key</returns>
        public float getFloat(string key) {
            string value = getString(key);
            if (value == null || !NumberHelper.isNumber(value)) {
                return 0;
            }

            return float.Parse(value);
        }

        /// <summary>
        /// Sets and saves a key and its float value to the config file
        /// </summary>
        /// <param name="key">The key that will be used to in future get the float value provided</param>
        /// <param name="value">The float value that will be returned when retrieved by the key</param>
        /// <returns>Instance of the current config class</returns>
        public Config setFloat(string key, float value) {
            return setString(key, value.ToString());
        }

        /// <summary>
        /// Gets a double from the config file using a key
        /// </summary>
        /// <param name="key">They key that we are using to find the double</param>
        /// <returns>The double that belongs to the key</returns>
        public double getDouble(string key) {
            string value = getString(key);
            if (value == null || !NumberHelper.isNumber(value)) {
                return 0;
            }

            return double.Parse(value);
        }

        /// <summary>
        /// Sets and saves a key and its double value to the config file
        /// </summary>
        /// <param name="key">The key that will be used to in future get the double value provided</param>
        /// <param name="value">The double value that will be returned when retrieved by the key</param>
        /// <returns>Instance of the current config class</returns>
        public Config setDouble(string key, double value) {
            return setString(key, value.ToString());
        }

        /// <summary>
        /// Gets a bool from the config file using a key
        /// </summary>
        /// <param name="key">They key that we are using to find the bool</param>
        /// <returns>The bool that belongs to the key</returns>
        public bool getBool(string key) {
            string value = getString(key);
            bool booleanValue;
            if (value == null || !bool.TryParse(value, out booleanValue)) {
                return false;
            }

            return booleanValue;
        }

        /// <summary>
        /// Sets and saves a key and its bool value to the config file
        /// </summary>
        /// <param name="key">The key that will be used to in future get the bool value provided</param>
        /// <param name="value">The bool value that will be returned when retrieved by the key</param>
        /// <returns>Instance of the current config class</returns>
        public Config setBool(string key, bool value) {
            return setString(key, value.ToString());
        }

        /// <summary>
        /// Gets a string from the config file using a key
        /// </summary>
        /// <param name="key">They key that we are using to find the string</param>
        /// <returns>The string that belongs to the key</returns>
        public string getString(string key) {
            if (getContents() == null) {
                return null;
            }

            string value = "";
            foreach (string contents in getContents()) {
                if (getKey(contents) == key) {
                    value = getValue(contents);
                    break;
                }
            }
            return value;
        }

        /// <summary>
        /// Sets and saves a key and its string value to the config file
        /// </summary>
        /// <param name="key">The key that will be used to in future get the string value provided</param>
        /// <param name="value">The string value that will be returned when retrieved by the key</param>
        /// <returns>Instance of the current config class</returns>
        public Config setString(string key, string value) {
            string configPath = key + fileSeperator + value;

            // Check to see if that value already exists, if it does, delete it
            foreach (string configData in new HashSet<string>(getContents())) {
                if (getKey(configData) == key) {
                    contents.Remove(configData);
                }
            }

            // Add the new key and its value to the config and then save it
            contents.Add(configPath);
            save();
            return this;
        }

        /// <summary>
        /// Check to see if the string provided is in the config format
        /// </summary>
        /// <param name="configData">The string that we want to check</param>
        /// <returns>True: String is a valid config path, False: String is not a config path</returns>
        private bool isConfigFormat(string configData) {
            return configData.Contains(fileSeperator);
        }

        /// <summary>
        /// Gets a key from a config data path
        /// </summary>
        /// <param name="configData">The string we want to get the key of</param>
        /// <returns>Key of the config data path</returns>
        public string getKey(string configData) {
            if (!isConfigFormat(configData)) {
                return null;
            }
            configData = configData.Split(fileSeperator.ToCharArray())[0].Replace(fileSeperator, "");
            return configData;
        }

        /// <summary>
        /// Gets the value of a config data path
        /// </summary>
        /// <param name="configData">The config path we want to get the value of</param>
        /// <returns>Value of the config data path</returns>
        public string getValue(string configData) {
            if (!isConfigFormat(configData)) {
                return null;
            }
            string[] paths = configData.Split(fileSeperator.ToCharArray());
            return paths[paths.Length - 1];
        }

        /// <summary>
        /// Gets the contents of the config file
        /// </summary>
        /// <returns>Contents of the config file</returns>
        public HashSet<string> getContents() {
            if (!File.Exists(getFilePath())) {
                File.Create(getFilePath()).Close();
            }
            if (contents == null || contents.Count == 0) {
                contents = new HashSet<string>(File.ReadAllLines(getFilePath()));
            }
            return contents;
        }

        /// <summary>
        /// Saves a config file to disk
        /// </summary>
        /// <returns>Instance of the current config class</returns>
        public virtual Config save() {
            if (getContents() == null) {
                return this;
            }
            File.Delete(getFilePath());

            string[] lines = new string[getContents().Count];
            getContents().CopyTo(lines, 0);
            File.WriteAllLines(getFilePath(), lines);

            return this;
        }
    }
}