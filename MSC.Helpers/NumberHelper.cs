﻿namespace MSC.Helpers {
    public class NumberHelper {

        /// <summary>
        /// See if a number
        /// </summary>
        /// <param name="toParse"></param>
        /// <returns></returns>
        public static bool isNumber(string toParse) {
            double number;
            return double.TryParse(toParse, out number);
        }

        /// <summary>
        /// Gets a float value of a string
        /// </summary>
        /// <param name="toParse">The string we want to get the float value of</param>
        /// <returns>Float value of a string</returns>
        public static float GetFloat(string toParse) {
            if(!isNumber(toParse)) {
                return 0;
            }
            return float.Parse(toParse);
        }

        /// <summary>
        /// Gets a int value of a string
        /// </summary>
        /// <param name="toParse">The string we want to get the int value of</param>
        /// <returns>Float value of a string</returns>
        public static int GetInt(string toParse) {
            if (!isNumber(toParse)) {
                return 0;
            }
            return int.Parse(toParse);
        }

    }
}
