﻿using System;
using System.Net.Sockets;
using System.Text;

namespace MSC.Helpers.Network {
    public class Client : MSCSocket {

        public string ClientID { get; protected set; }

        public string Username { get; protected set; }

        private byte[] serverResponce = new byte[1024];

        public Client(string username, string ip, int port) : base(ip, port) {
            Username = username;
            ClientID = Guid.NewGuid().ToString();
        }

        public override void Connect() {
            base.Connect();
            Socket.Connect(IPEndPoint);
            if (Socket.Connected) {
                SendData("Client connected with ClientID '" + ClientID + "' and username '" + Username + "'");
            }
        }

        public override bool CloseConnection() {
            if (Socket.Connected) {
                SendData("Client '" + ClientID + " (Username: " + Username + ") disconnected!");
            }
            return base.CloseConnection();
        }

        public string SendData(string data) {
            if (Socket == null || !Socket.Connected) {
                return null;
            }
            try {
                byte[] message = Encoding.Unicode.GetBytes(data);

                int bytesSent = Socket.Send(message);
                return RecieveData();
            } catch {
                return null;
            }
        }

        public string RecieveData() {
            string responce = null;

            try {
                int bytesRec = Socket.Receive(serverResponce);

                responce = Encoding.Unicode.GetString(serverResponce, 0, bytesRec);

                while (Socket.Available > 0) {
                    bytesRec = Socket.Receive(serverResponce);
                    responce += Encoding.Unicode.GetString(serverResponce, 0, bytesRec);
                }
            } catch {
                // Do nothing here as its the client side and we want it to be pretty!
            }

            return responce;
        }

    }
}
