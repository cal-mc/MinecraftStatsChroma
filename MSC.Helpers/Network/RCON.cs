﻿using MinecraftServerRCON;
using MSC.Helpers.Events;

namespace MSC.Helpers.Network {
    public class RCON {

        public string IP { get; set; }

        public int Port { get; set; }

        public string Password { get; set; }

        private RconEvent rconEvent;

        public RCON(string ip, int port, string password)  {
            IP = ip;
            Port = port;
            Password = password;

            rconEvent = new RconEvent(this);
        }


        /// <summary>
        /// Sends a command to a minecraft server
        /// </summary>
        /// <param name="command">The command that we are sending</param>
        /// <returns>Command feedback</returns>
        public string SendCommand(string command) {
            string response = null;

            try {
                using (var server = RCONClient.INSTANCE) {
                    server.setupStream(IP, Port, Password);
                    response = server.sendMessage(RCONMessageType.Command, command).RemoveColorCodes();
                }
            } catch {
                // ignored
            }

            rconEvent.Fire(response);
            return response;
        }

        /// <summary>
        /// Sends a command to a minecraft server
        /// </summary>
        /// <param name="command">The command that we are sending</param>
        /// <param name="ip">The IP of the server we want to send the command to</param>
        /// <param name="port">The port fo the server we want to send the command to</param>
        /// <param name="password">RCON password for the server</param>
        /// <returns>Command feedback</returns>
        public static string SendCommand(string command, string ip, int port, string password) {
            return new RCON(ip, port, password).SendCommand(command);
        }

    }
}
