﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace MSC.Helpers.Network {
    public class MSCSocket {

        public MSCSocket(string ip, int port) {
            IP = ip;
            Port = port;
            Permission = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, ip, port);
        }

        public string IP { get; protected set; }

        public int Port { get; protected set; }

        public SocketPermission Permission { get; protected set; }

        public Socket Socket { get; protected set; }

        public IPEndPoint IPEndPoint { get; protected set; }

        public Socket Handler { get; protected set; }

        public virtual void Connect() {
            //IPHostEntry ipHost = Dns.GetHostEntry(IP);
            //IPAddress ipAddress = ipHost.AddressList[0];
            IPEndPoint = new IPEndPoint(IPAddress.Parse(IP), Port);

            Permission.Demand();

            Socket = new Socket(IPEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public virtual bool CloseConnection() {
            try {
                if (Socket.Connected) {
                    Socket.Shutdown(SocketShutdown.Receive);
                    Socket.Close();
                }
                return true;
            } catch {
                return false;
            }
        }

        public bool ServerStatus() {
            if (Socket.Connected) {
                Console.WriteLine("Server listening for incoming connections on " + IP + ":" + Port);
                return true;
            }
            Console.WriteLine("Server is not connected!");
            return false;
        }

    }
}
