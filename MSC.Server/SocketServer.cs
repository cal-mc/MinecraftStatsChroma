﻿using System;
using System.Text;
using System.Net.Sockets;
using MSC.Helpers.Network;
using Corale.Colore.Core;

namespace MSC.Server {
    class SocketServer : MSCSocket {

        // TODO: Make this read the server.properties file (Not minecraft one, the actutal MSC one!)
        private static int MaxClients { get; set; }

        private string RCONIP { get; set; }

        private int RCONPort { get; set; }

        private string RCONPassword { get; set; }

        public SocketServer(string ip, int port, string rconIP, int rconPort, string rconPassword, int maxCients) : base(ip, port) {
            RCONIP = rconIP;
            RCONPort = rconPort;
            RCONPassword = rconPassword;
            MaxClients = maxCients;
        }

        public override void Connect() {
            base.Connect();
            Socket.Bind(IPEndPoint);

            Console.WriteLine("Server connected on " + IP + ":" + Port);
        }

        public override bool CloseConnection() {
            if (Socket.Connected) {
                SendMessage("Server closed!");
            }
            return base.CloseConnection();
        }

        public void Listen() {
            try {
                Socket.Listen(MaxClients);

                AsyncCallback asyncCallback = new AsyncCallback(AcceptCallback);
                Socket.BeginAccept(asyncCallback, Socket);
            } catch (Exception ex) {
                if (ServerProperties.INSTANCE.Debug) {
                    Console.WriteLine("ERROR WHILE TRYING TO LISTEN:\n" + ex.Message);
                }
            }
        }

        public void AcceptCallback(IAsyncResult ar) {
            Socket listener = null;

            try {
                // Create a buffer for the datat that we will recieve
                byte[] buffer = new byte[1024];

                // Get the listeneing socket object from the callback
                listener = (Socket)ar.AsyncState;

                // Create a new socket from the callback
                Handler = listener.EndAccept(ar);

                // Uses the Nagle algorithm.. I think
                Handler.NoDelay = false;

                BeginReceive();

                AsyncCallback asyncCallback = new AsyncCallback(AcceptCallback);
                listener.BeginAccept(asyncCallback, listener);
            } catch (Exception ex) {
                if (ServerProperties.INSTANCE.Debug) {
                    Console.WriteLine("ERROR WHILE ACCEPTING CALLBACK:\n" + ex.Message);
                }
            }
        }

        public void ReceiveCallback(IAsyncResult ar) {
            try {
                // Fetch data from callback
                object[] data = new object[2];
                data = (object[])ar.AsyncState;

                // The buffer that contains the data
                byte[] buffer = (byte[])data[0];

                // Socket the handle host communication
                Handler = (Socket)data[1];

                // Will hold the message the was received
                string message = string.Empty;

                // Number of bytes recivied
                int bytesRead = Handler.EndReceive(ar);

                if (bytesRead > 0) {
                    message += Encoding.Unicode.GetString(buffer, 0, bytesRead);
                    if (ServerProperties.INSTANCE.Debug) {
                        Console.WriteLine(message);
                        SendMessage(message);
                    }
                    if (message.Contains("RCON COMMAND:")) {
                        SendMessage(Server.SendRCON(message.Split(new string[] { "RCON COMMAND:" }, StringSplitOptions.None)[1]));
                    }
                }


                // Get a way to cancel the event...:S
                BeginReceive();

            } catch (Exception ex) {
                if (ServerProperties.INSTANCE.Debug) {
                    Console.WriteLine("ERROR WHILE RECIEVING CALLBACK:\n" + ex.Message);
                }
            }
        }

        private void BeginReceive() {
            object[] data = new object[2];

            byte[] buffer = new byte[1024];
            data[0] = buffer;
            data[1] = Handler;

            Handler.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), data);
        }


        public bool SendMessage(string message) {
            if (Handler == null || !Handler.Connected) {
                return false;
            }
            try {
                // Prepare the reply
                byte[] data = Encoding.Unicode.GetBytes(message);

                // Send the message to the connected socket
                Handler.BeginSend(data, 0, data.Length, 0, new AsyncCallback(SendCallback), Handler);
                return true;
            } catch (Exception ex) {
                if (ServerProperties.INSTANCE.Debug) {
                    Console.WriteLine("ERROR WHILE SENDING DATA:\n" + ex.Message);
                }
                return false;
            }
        }

        public void SendCallback(IAsyncResult ar) {
            try {
                Socket handler = (Socket)ar.AsyncState;

                int bytesSend = handler.EndSend(ar);

                if (ServerProperties.INSTANCE.Debug) {
                    ConsoleColor before = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("Sending" + bytesSend + " (bytes) to the client!");
                    Console.ForegroundColor = before;
                }
            } catch { }
        }

    }
}
