﻿using System;
using MSC.Helpers.Network;

namespace MSC.Server {
    class Server {

        private static SocketServer server;

        private static ConsoleColor defaultColor = ConsoleColor.Green;

        private static ServerProperties ServerProperties { get; set; }

        static void Main(string[] args) {
            SetColor(defaultColor);
            ServerProperties = new ServerProperties();

            server = new SocketServer(ServerProperties.ServerIP, ServerProperties.ServerPort, ServerProperties.RconIP, ServerProperties.RconPort, ServerProperties.RconPassword, ServerProperties.MaxClients);

            server.Connect();
            server.Listen();

            ProcessCommand();
        }

        /// <summary>
        /// Manages commands
        /// </summary>
        public static void ProcessCommand() {
            Console.Write("> ");
            string command = Console.ReadLine().TrimStart().TrimEnd().ToLower();

            switch (command) {
                case "close":
                case "quit":
                case "exit":
                    server.CloseConnection();
                    return;
                case "?":
                case "help":
                    SetColor(ConsoleColor.Cyan);
                    Console.WriteLine(GetHelp());
                    SetColor(defaultColor);
                    break;
                case "cls":
                case "clear":
                    Console.Clear();
                    break;
                case "minecraft":
                case "servercommand":
                case "server":
                case "mc":
                case "rcon":
                    SetColor(ConsoleColor.Cyan);
                    Console.Write("Type a command you wish to send to the server:\n/");
                    SetColor(ConsoleColor.Gray);
                    string feedback = RCON.SendCommand(Console.ReadLine().TrimStart().TrimEnd(), ServerProperties.RconIP, ServerProperties.RconPort, ServerProperties.RconPassword);
                    if (feedback != "") {
                        SetColor(ConsoleColor.Yellow);
                        Console.WriteLine("\n" + feedback);
                    }
                    SetColor(defaultColor);
                    break;
                case "status":
                    if (server.ServerStatus()) {
                        SetColor(ConsoleColor.Cyan);
                        Console.WriteLine("Server is connected on " + ServerProperties.ServerIP + ":" + ServerProperties.ServerPort + "!");
                        SetColor(defaultColor);
                    } else {
                        SetColor(ConsoleColor.Red);
                        Console.WriteLine("Socket server is not connected!!");
                        SetColor(defaultColor);
                    }
                    break;
                default:
                    SetColor(ConsoleColor.Red);
                    Console.WriteLine("Command not found!\nType 'help' for a list of commands.");
                    SetColor(defaultColor);
                    break;
            }

            ProcessCommand();
        }

        public static string SendRCON(string command) {
            return RCON.SendCommand(command, ServerProperties.RconIP, ServerProperties.RconPort, ServerProperties.RconPassword);
        }

        public static string GetHelp() {
            string help = string.Empty;

            string spacer = "\t-    ";

            help += "Exit" + spacer + "Closes the server down safely\n";
            help += "Help" + spacer + "Shows this help file\n";
            help += "Clear" + spacer + "Clears the console screen of all text\n";
            help += "Rcon" + spacer + "Sends a command to the minecraft server";

            return help;
        }

        public static void SetColor(ConsoleColor color) {
            Console.ForegroundColor = color;
        }

    }
}
