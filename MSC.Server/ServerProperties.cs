﻿using MSC.Helpers;

namespace MSC.Server {
    class ServerProperties : Config {

        public static ServerProperties INSTANCE = null;

        // The IP that this server will use for clients to connect to it
        public string ServerIP { get; private set; }

        // The port that this server will use for clients to connect to it
        public int ServerPort { get; private set; }

        // The IP that the RCON server is running on
        public string RconIP { get; private set; }

        // The port that the RCON server is running on
        public int RconPort { get; private set; }
        
        // The RCON password that is used to connect to the external minecraft server
        public string RconPassword { get; private set; }

        // The maximum amount of clients that can connect to the server
        public int MaxClients { get; private set; }

        public bool Debug { get; private set; }

        public ServerProperties() : base("server.properties") {
            ServerIP = (getString("ServerIP") != null) ? getString("ServerIP") : "127.0.0.1";
            ServerPort = (getInt("ServerPort") == 0) ? 1234 : getInt("ServerPort");

            RconIP = (getString("RconIP") != null) ? getString("RconIP") : "127.0.0.1";
            RconPort = (getInt("RconPort") == 0) ? 25570 : getInt("RconPort");
            RconPassword = (getString("RconPassword") != null) ? getString("RconPassword") : "";

            MaxClients = (getInt("MaxClients") == 0) ? 1 : getInt("MaxClients");

            Debug = (getString("Debug") != null) ? getBool("Debug") : false;

            INSTANCE = this;
        }

        /// <summary>
        /// Saves the config file
        /// </summary>
        /// <returns>Instance of the config class</returns>
        public override Config save() {
            if(!configExists()) {
                // Socket server information
                setString("ServerIP", "127.0.0.1");
                setInt("ServerPort", 1234);

                // RCON server information
                setString("RconIP", "127.0.0.1");
                setInt("RconPort", 25570);
                setString("RconPassword", "");

                // General info
                setBool("Debug", false);
            }
            return base.save(); ;
        }
    }
}
